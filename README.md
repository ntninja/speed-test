# SpeedTest #

 – A simple and reliable speed testing application.

## Methodology ##

This test measures the actually throughoutput between your browser and the
given HTTP server and does not take the overhead of TCP/IP into consideration.
While this might be somewhat unclean from a technical perspective, it answers
the question you (as a user) actually care about:

How fast can I download and upload a file on my current connection?

A public instance of this speed tester may be found here:  
https://xmine128.tk/SpeedTest/

## System requirements ##

 * Browser with `window.crypto` and `XMLHttpRequest.upload` support
    - Confirmed to work on Android 4.4 stock browser, FireFox mobile,
      FireFox desktop and Chrome
 * Web server that can server static files
 * (Optionally) PHP to dynamically generate the download data

## Usage ##

If your server is behind a proxy please make sure that the proxy allows
the upload of sufficiently large files (otherwise the numbers will be bogus).

Under `nginx` this can be done by adding:

```
	client_max_body_size 0;
```

before your `proxy_pass` directive.

### With PHP enabled ###

 1. Clone git repository to public web server directory:  
    `git clone https://gitlab.com/alexander255/speed-test.git`
 2. Done

### Without PHP ###

 1. Clone git repository to public web server directory:
    `git clone https://gitlab.com/alexander255/speed-test.git`
 2. Pre-generate download chunks:  
    * `dd if=/dev/urandom of=chunk_1000.dat      bs=1000 count=1`
    * `dd if=/dev/urandom of=chunk_10000.dat     bs=1000 count=10`
    * `dd if=/dev/urandom of=chunk_100000.dat    bs=1000 count=100`
    * `dd if=/dev/urandom of=chunk_1000000.dat   bs=1000 count=1000`
    * `dd if=/dev/urandom of=chunk_10000000.dat  bs=1000 count=10000`
    * `dd if=/dev/urandom of=chunk_100000000.dat bs=1000 count=100000`
 3. Tweak `config.json`:
    * Change `"url"` in `"download"` to `"chunk_{SIZE}.dat"`
    * Change `"url"` in `"upload"` to `"chunk_1000.dat"`
 4. Done


