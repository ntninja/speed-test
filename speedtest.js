window.SpeedTest = (function()
{
	var registerCallbacks = function SpeedTest_registerCallbacks
			(eventTarget, cb_start, cb_progress, cb_done, config, size, func)
	{
		var time_start;
		
		// Report test start
		eventTarget.addEventListener('loadstart', function(event)
		{
			// Call start-callback
			cb_start(size);
			
			// Capture start time (in ms)
			time_start = (new Date()).getTime();
		});
		
		// Monitor progress
		eventTarget.addEventListener('progress', function(event)
		{
			// Capture current progress time (in ms)
			var passed = ((new Date()).getTime() - time_start) / 1000;
			
			// Determine total size and transfered size
			var loaded = event.loaded;
			
			// Call callback
			cb_progress(size, passed, loaded);
		}, false);
		
		// Report test end
		eventTarget.addEventListener('load', function(event)
		{
			var passed = ((new Date()).getTime() - time_start) / 1000;
			
			// Call the progress callback one last time,
			// since the last progress-Event isn't always emitted
			cb_progress(size, passed, size);
			
			if(passed < config.time && (size * 10) <= config.limit) {
				cb_done(size, passed, 'toofast');
				
				// Download has been too fast - Retry it with a bigger file size
				func(cb_start, cb_progress, cb_done, config, size * 10);
			} else {
				cb_done(size, passed, 'success');
			}
		});
		eventTarget.addEventListener('abort', function(event)
		{
			var passed = ((new Date()).getTime() - time_start) / 1000;
			
			cb_done(size, passed, 'fail', 'aborted');
		});
		eventTarget.addEventListener('error', function(event)
		{
			var passed = ((new Date()).getTime() - time_start) / 1000;
			
			cb_done(size, passed, 'fail', 'error');
		});
	}
	
	
	var deepCopy = function SpeedTest_deepCopy(obj) {
		if(obj instanceof Array) {
			var result = [];
			for(var i = 0, len = obj.length; i < len; i++) {
				result[i] = SpeedTest_deepCopy(obj[i]);
			}
			return result;
		} else if(obj != null && typeof(obj) === 'object') {
			var result = {};
			for(var i in obj) {
				// Not using .hasOwnProperty() is intensional here!!!
				result[i] = SpeedTest_deepCopy(obj[i]);
			}
			return result;
		} else {
			return obj;
		}
	}
	
	
	var makeConfig = function SpeedTest_makeConfig(scope, config)
	{
		// Use global configuration of there was no other provided
		if(config == null) {
			config = window.SpeedTest.config;
		// Use original configuration instead of modified one
		} else if(config._original) {
			config = config._original;
		}
		
		// Clone configuration
		var config2 = deepCopy(config);
		
		// Keep reference to original configuration
		config2._original = config;
		
		// Apply scope specific overrides
		for(var name in config2[scope]) {
			if(config2[scope].hasOwnProperty(name)) {
				config2[name] = config2[scope][name];
			}
		}
		
		// Return new configuration
		return config2;
	}
	
	
	return {
		download: function SpeedTest_download
				(cb_start, cb_progress, cb_done, config, _size)
		{
			// Start at 1kB download
			_size = (typeof(_size) == 'number') ? _size : 1000;
			
			// Process configuration parameters
			config = makeConfig('download', config);
			
			
			// Generate URL
			var url = config.url.replace(/[{]size[}]/i, _size);
			url += ((url.indexOf('?') > -1) ? '&' : '?');
			url += (new Date()).getTime();
			
			// Open server connection
			var client = new XMLHttpRequest();
			client.open('GET', url, true);
			
			// Register callbacks
			registerCallbacks(
					client,
					cb_start,
					cb_progress,
					cb_done,
					config,
					_size,
					SpeedTest_download
			);
		
			// Send request
			client.send();
		},
		upload: function SpeedTest_upload
				(cb_start, cb_progress, cb_done, config, _size)
		{
			// Start at 1kB upload
			_size  = (typeof(_size) == 'number') ? _size : 1000;
			
			// Process configuration parameters
			config = makeConfig('upload', config);
			
			
			// Generate URL
			var url = config.url.replace(/[{]size[}]/i, _size);
			url += ((url.indexOf('?') > -1) ? '&' : '?');
			url += (new Date()).getTime();
			
			// Open server connection
			var client = new XMLHttpRequest();
			client.open('POST', url, true);
			
			// Register callbacks
			registerCallbacks(
					client.upload,
					cb_start,
					cb_progress,
					cb_done,
					config,
					_size,
					SpeedTest_upload
			);
			
			// Create ArrayBuffer for storage of upload data
			var buffer = new ArrayBuffer(_size);
			
			// Fill buffer with random data (if requested)
			if(config.random && window.crypto && window.crypto.getRandomValues) {
				// Work around QuotaExceededError:
				var i = 0;
				for(; i < _size; i += 1000) {
					window.crypto.getRandomValues(new Uint8Array(buffer, i, 1000));
				}
				if(i > _size) {
					window.crypto.getRandomValues(new Uint8Array(buffer, i, i - _size));
				}
				delete i;
			}
			
			// Upload data
			client.send(buffer);
		}
	};
})();
