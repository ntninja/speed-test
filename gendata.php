<?php
	##########################################
	# Generate the requested number of bytes #
	##########################################
	
	function json_config_parse($filepath)
	{
		# Open file
		$fileobj = fopen($filepath, 'r');
		
		$text = '';
		
		# Strip JSONP and comment lines
		$strip = false;
		while(($line = fgets($fileobj)) !== FALSE) {
			// Process non-JSON section tags
			if(strpos(strtolower($line), '<nojson>') > -1) {
				$strip = true;
			}
			if(strpos(strtolower($line), '</nojson>') > -1) {
				$strip = false;
			}
			
			// Strip non-JSON lines
			if($strip || strpos(trim($line), '//') === 0) {
				continue;
			}
			
			// Use all other lines in config
			$text .= "${line}";
		}
		
		// Parse JSON
		$json = json_decode($text, true);
		
		// Handle errors
		if(($error = json_last_error()) !== JSON_ERROR_NONE) {
			header('HTTP/1.0 400 Bad Request');
			header('Content-Type: text/plain; charset=UTF-8');
			
			switch($error) {
				case JSON_ERROR_DEPTH:
					$message = 'Maximum stack depth exceeded';
				break;
				case JSON_ERROR_STATE_MISMATCH:
					$message = 'Underflow or modes mismatch';
				break;
				case JSON_ERROR_CTRL_CHAR:
					$message = 'Unexpected control character found';
				break;
				case JSON_ERROR_SYNTAX:
					$message = 'Syntax error';
				break;
				case JSON_ERROR_UTF8:
					$message = 'Malformed UTF-8 characters';
				break;
				default:
					$message = "Unknown error ($error)";
			}
			
			die("Failed to parse configuration file: {$message}");
		}
		
		# Return parsed JSON
		return $json;
	}
	
	# Parse configuration and apply download-specific overrides
	$config = json_config_parse('config.json');
	$config = array_merge($config, $config['download']);



	# Make sure parameter was provided
	if(!isset($_GET['size'])) {
		header('HTTP/1.0 400 Bad Request');
		header('Content-Type: text/plain; charset=UTF-8');

		die("Parameter error: 'size' missing");
	}

	# Read number of bytes
	$size = intval($_GET['size']);

	# Make sure the maximum size is respected
	if($size > $config['limit']) {
		header('HTTP/1.0 400 Bad Request');
		header('Content-Type: text/plain; charset=UTF-8');

		die("Parameter error: 'size' > {$config['limit']}");
	}

	# Send data
	header('HTTP/1.0 200 OK');
	header('Content-Type: text/plain; charset=UTF-8');

	# Prevent gzipping and buffering of content
	ini_set('output_buffering',        0);
	ini_set('implicit_flush',          1);
	ini_set('zlib.output_compression', 0);
	if(function_exists('apache_setenv')) {
		apache_setenv('no-gzip', 1);
	}
	while(ob_get_level() > 0) {
		ob_end_flush();
	}
	ob_implicit_flush(1);
	
	if(!$config['random']) {
		for($i = 0; $i < $size; $i += 1000) {
			print(str_repeat(' ', 1000));
			flush();
		}
		if($i > $size) {
			print(str_repeat(' ', $i - $size));
		}
	} elseif(is_readable('/dev/urandom')) {
		$urandom = fopen('/dev/urandom', 'r');
		for($i = 0; $i < $size; $i += 1000) {
			print(fread($urandom, 1000));
			flush();
		}
		if($i > $size) {
			print(fread($urandom, $i - $size));
		}
		fclose($urandom);
	} else {
		for($i = 0; $i < $size; $i += 1000) {
			print(openssl_random_pseudo_bytes(1000));
			flush();
		}
		if($i > $size) {
			print(openssl_random_pseudo_bytes($i - $size));
		}
	}
		

	exit(0);
